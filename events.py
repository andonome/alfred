class Subject():
    observers = []

    def __call__(self, *args):
        for observer in self.observers:
            observer(*args)

    def subscribe(self, observer):
        self.observers.append(observer)

    def unsubscribe(self, observer):
        self.observers.remove(observer)

    def unsubscribe_all(self):
        self.observers = []
