# Plans

+ Change to Python
+ Make it read a configuration file of arbitrary length, with a combination of mac addresses (to identify people) and resulting actions (usually 'play this song').

# English notation

+ arp-scan and keep the results as 'scans'
+ read the file 'people':
+ If you find $1 is above 100 (i.e. the person has been away for a long time) then they are 'ready'.
+ Each time you find $2 (mac addresses) reset $1 (person's name variable) to 0, otherwise add +1 to $1.
+ If $1 == 100 (i.e. the person is away) then execute $4 (leaving conditions)

# People List

The list contains 4 items:

+ The person's name, used as a variable to show if they're in.
+ The person's mac address.
+ The action to take when the person enters (defaults will be provided).
+ The action to take when the person leaves (defaults will be provided)

`bob,"83:84:s8:83:z7","play bob's playlist","remove bob's playlist"`

`alice,"u3:84:s8:83:z7","play alice's playlist","remove alice's playlist"`

`mike,"r3:94:s8:83:z7","download news for mic",""`


