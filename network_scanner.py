import os
import re


class ScanError(Exception):
    def __init__(self, message):
        self.message = message


def scan():
    if os.getuid() != 0:
        raise ScanError("Alfred uses arp-scan and requires root privileges.")
    if not os.path.isfile("/usr/bin/arp-scan"):
        raise ScanError("arp-scan not found.")
    output = os.popen("arp-scan -l").read().split("\n")[2:-3]
    addrs = []
    for line in output:
        pattern = re.compile("(([a-f\d]{2}:){5}[a-f\d]{2})")
        result = re.search(pattern, line)
        if result is not None:
            addrs.append(result.groups()[0])
    return addrs
