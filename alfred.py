#!/usr/bin/env python3

import network_scanner as ns

hosts = {}
timeout = 100

if __name__ == "__main__":
    try:
        while True:
            macs = set(ns.scan())
            for mac in macs:
                if not mac in hosts:
                    print(mac + ' joined')
                hosts[mac] = timeout
            for host in hosts.keys():
                hosts[host] -= 1
                if hosts[host] == 0:
                    del hosts[host]
                    print(host + ' left')
            print(hosts)
    except ns.ScanError as e:
        print(e.message)
        exit(0)
