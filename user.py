import pickle
from events import Subject


class User:
    on_enter = Subject()
    on_leave = Subject()
    macs = []

    def __init__(self, username, macs, enter_actions='', leave_actions=''):
        self.username = username
        self.macs = macs
        self.enter_actions = enter_actions
        self.leave_actions = leave_actions


users_path = 'users'


def save_users(users):
    f = open(users_path, 'wb')
    pickle.dump(users, f)


def load_users():
    f = open(users_path, 'rb')
    return pickle.load(f)
